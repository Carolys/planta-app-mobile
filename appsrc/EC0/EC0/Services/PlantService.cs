﻿using EC0.Cache;
using EC0.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RestSharp;
using SimpleJSON;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace EC0.Services
{
    public class PlantService
    {
        protected const string DefaultBaseUrl = "https://planta-server.loca.lt/api/";
        protected SuccessfulAnswer ObjSuccessfulAnswer;
        public MemoryCacheSystem Cache { get; set; }
        public PlantService(MemoryCacheSystem cache)
        {
            this.Cache = cache;
        }

        public BaseResponse<Plant> GetPlant(bool acceptCache, int plantId)
        {
            return GetPlantAsync(acceptCache, plantId).Result;
        }

        public async Task<BaseResponse<Plant>> GetPlantAsync(bool acceptCache, int plantId)
        {
            try
            {
                if (acceptCache)
                {
                    if (Cache.Plants != null)
                    {
                        var cacheResponse = new BaseResponse<Plant>
                        {
                            Body = Cache.Plants.Find(x => x.Id == plantId)
                        };
                        return cacheResponse;
                    }
                }

                var request = new HttpRequestMessage
                {
                    RequestUri = createRequestUri("plant/" + plantId.ToString()),
                    Method = HttpMethod.Get
                };
                var response = await ExecuteRequestAsync<Plant>(request).ConfigureAwait(false);
                return response;
            }
            catch (Exception ex)
            {
                ObjSuccessfulAnswer = new SuccessfulAnswer() { TitleMessage = "Ops, erro na requisição da planta!", Message = ex.Message, Success = false };
                return null;
            }

        }

        private async Task<BaseResponse<T>> ExecuteRequestAsync<T>(HttpRequestMessage request) where T : new()
        {

            using (var httpClient = new HttpClient(new HttpClientHandler()))
            {
                var httpResponseMessage = await httpClient.SendAsync(request).ConfigureAwait(false);

                var content = await httpResponseMessage.Content.ReadAsStringAsync().ConfigureAwait(false);

                var response = new BaseResponse<T>
                {
                    HttpStatus = httpResponseMessage.StatusCode,
                    IsOk = httpResponseMessage.IsSuccessStatusCode,
                    ReasonPhrase = httpResponseMessage.ReasonPhrase
                };

                if (content != null)
                {
                    if (httpResponseMessage.IsSuccessStatusCode)
                    {
                        response.Body = JsonConvert.DeserializeObject<T>(content);
                    }
                    else
                    {
                        var error = (BaseResponse)Convert.ChangeType(content, typeof(BaseResponse));
                        if (error != null)
                        {
                            response.StatusCode = error.StatusCode;
                            response.StatusMessage = error.StatusMessage;
                        }
                    }
                }

                Debug.WriteLine(response);
                return response;
            }

        }

        private Uri createRequestUri(string resource, params Object[] args)
        {
            StringBuilder builder = new StringBuilder();
            builder.Append(DefaultBaseUrl);

            if (args != null)
            {
                builder.AppendFormat(resource, args);
            }
            else
            {
                builder.Append(resource);
            }


            string url = builder.ToString();
            return new Uri(url);
        }
    }
}
