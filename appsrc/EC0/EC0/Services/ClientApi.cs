﻿using EC0.Cache;
using EC0.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace EC0.Services
{
    public class ClientApi
    {
        protected const string DefaultBaseUrl = "https://planta-server.loca.lt/api";

        protected SuccessfulAnswer ObjSuccessfulAnswer;
        public MemoryCacheSystem Cache { get; set; }
        public PlantService PlantService { get; set; }

        public ClientApi()
        {
            Cache = new MemoryCacheSystem();
            PlantService = new PlantService(Cache);
        }
    }
}
