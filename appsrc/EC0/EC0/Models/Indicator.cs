﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace EC0.Models
{
    public class Indicator
    {
        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("plant_id")]
        public int Plant_id { get; set; }

        [JsonProperty("type")]
        public Type Type { get; set; }

        [JsonProperty("value")]
        public int Value { get; set; }

        [JsonProperty("created_at")]
        public string Created_at { get; set; }
        public DateTime DateTime_Created_at { get; set; }

        [JsonProperty("updated_at")]
        public string Updated_at { get; set; }
        public DateTime DateTime_Updated_at { get; set; }

    }
}
