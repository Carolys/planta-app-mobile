﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace EC0.Models
{
    public enum Type
    {
        [JsonProperty("humidity")]
        Humidity,

        [JsonProperty("temperature")]
        Temperature
    }
}
