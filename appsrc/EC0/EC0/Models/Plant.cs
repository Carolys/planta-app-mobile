﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace EC0.Models
{
    public class Plant
    {
        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("birthday")]
        public string Birthday { get; set; }

        [JsonProperty("license")]
        public string License { get; set; }

        [JsonProperty("enabled")]
        public int Enabled { get; set; }

        [JsonProperty("user_id")]
        public int? User_id { get; set; }

        [JsonProperty("next_run_in_minutes")]
        public int Next_run_in_minutes { get; set; }

        [JsonProperty("last_ran_at")]
        public string Last_ran_at { get; set; }

        [JsonProperty("created_at")]
        public string Created_at { get; set; }

        [JsonProperty("updated_at")]
        public string Updated_at { get; set; }

        [JsonProperty("error")]
        public string Error { get; set; }

        [JsonProperty("indicators")]
        public List<Indicator> Indicators = new List<Indicator>();

    }
}
