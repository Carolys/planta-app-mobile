﻿using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net;
using System.Text;

namespace EC0.Models
{
    [DebuggerDisplay("HttpStatus={HttpStatus}; StatusCode={StatusCode}; StatusMessage={StatusMessage}")]
    public class BaseResponse
    {
        [JsonProperty("statusCode")]
        public HttpStatusCode StatusCode { get; set; }
        [JsonProperty("status_message")]
        public string StatusMessage { get; set; }
        public string Error { get; set; }
        public ResponseStatus ResponseStatus { get; set; }
        public HttpStatusCode HttpStatus { get; set; }
        public bool IsOk { get; set; }
        public bool IsSuccessful { get; set; }
        public string ReasonPhrase { get; set; }
        public string CodeMessage { get; set; }
    }

    public class BaseResponse<T> : BaseResponse
    {
        public T Body { get; set; }

        public BaseResponse() { }

        public BaseResponse(T body)
        {
            Body = body;
        }

        //public overridestring ToString()
        //{
        //    return string.Format("HttpStatus={0}; StatusCode={1}; StatusMessage={2}; Body={3}", HttpStatus, StatusCode, StatusMessage, Body);
        //}
    }
}

