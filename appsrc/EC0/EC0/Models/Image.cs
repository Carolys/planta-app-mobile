﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EC0.Models
{
    public class Image
    {
        public string Date { get; set; }
        public string Source { get; set; }
    }
}
