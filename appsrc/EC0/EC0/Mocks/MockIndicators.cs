﻿using EC0.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace EC0.Mocks
{
    public class MockIndicators
    {
        public List<Indicator> getData()
        {
            var List = new List<Indicator>();

            List.Add(new Indicator
            {
                Id = 1,
                Plant_id = 1,
                Type = Models.Type.Humidity,
                Value = 20,
                Created_at = "2020-11-12T23:18:03.000000Z",
                Updated_at = "2020-11-12T23:18:03.000000Z",
            }); ;
            List.Add(new Indicator
            {
                Id = 1,
                Plant_id = 1,
                Type = Models.Type.Humidity,
                Value = 21,
                Created_at = "2020-11-13T23:18:03.000000Z",
                Updated_at = "2020-11-13T23:18:03.000000Z",
            });
            List.Add(new Indicator
            {
                Id = 1,
                Plant_id = 1,
                Type = Models.Type.Humidity,
                Value = 25,
                Created_at = "2020-11-14T23:18:03.000000Z",
                Updated_at = "2020-11-14T23:18:03.000000Z",
            });
            List.Add(new Indicator
            {
                Id = 1,
                Plant_id = 1,
                Type = Models.Type.Humidity,
                Value = 100,
                Created_at = "2020-11-18T23:18:03.000000Z",
                Updated_at = "2020-11-18T23:18:03.000000Z",
            });
            List.Add(new Indicator
            {
                Id = 1,
                Plant_id = 1,
                Type = Models.Type.Humidity,
                Value = 80,
                Created_at = "2020-11-18T23:18:03.000000Z",
                Updated_at = "2020-11-18T23:18:03.000000Z",
            });
            List.Add(new Indicator
            {
                Id = 1,
                Plant_id = 1,
                Type = Models.Type.Temperature,
                Value = 25,
                Created_at = "2020-11-12T23:18:03.000000Z",
                Updated_at = "2020-11-12T23:18:03.000000Z",
            });
            List.Add(new Indicator
            {
                Id = 1,
                Plant_id = 1,
                Type = Models.Type.Temperature,
                Value = 22,
                Created_at = "2020-11-12T23:17:00.000000Z",
                Updated_at = "2020-11-12T23:17:00.000000Z",
            });
            List.Add(new Indicator
            {
                Id = 1,
                Plant_id = 1,
                Type = Models.Type.Temperature,
                Value = 19,
                Created_at = "2020-11-12T23:17:03.000000Z",
                Updated_at = "2020-11-12T23:17:03.000000Z",
            });


            return List;
        }
    }
}
