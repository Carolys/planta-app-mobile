﻿using EC0.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

namespace EC0.Mocks
{
    public class MockImages
    {        
        public ObservableCollection<Image> getData()
        {
            var ObservableCollectiona = new ObservableCollection<Image>();

            ObservableCollectiona.Add(new Image
            {
                Date = "12/03/2020",
                Source = "image1.png"
            });
            ObservableCollectiona.Add(new Image
            {
                Date = "12/05/2020",
                Source = "image2.png"
            });
            ObservableCollectiona.Add(new Image
            {
                Date = "25/10/2020",
                Source = "image3.png"
            });
            ObservableCollectiona.Add(new Image
            {
                Date = "11/11/2020",
                Source = "image4.png"
            });
            ObservableCollectiona.Add(new Image
            {
                Date = "05/11/2020",
                Source = "image5.png"
            });


            return ObservableCollectiona;
        }
    }
}
