using Prism;
using Prism.Ioc;
using EC0.ViewModels;
using EC0.Views;
using Xamarin.Essentials.Interfaces;
using Xamarin.Essentials.Implementation;
using Xamarin.Forms;

namespace EC0
{
    public partial class App
    {
        public App(IPlatformInitializer initializer)
            : base(initializer)
        {
        }

        protected override async void OnInitialized()
        {
            InitializeComponent();

            await NavigationService.NavigateAsync("/AppNavigationPage/LicensePage");
        }

        protected override void RegisterTypes(IContainerRegistry containerRegistry)
        {
            containerRegistry.RegisterSingleton<IAppInfo, AppInfoImplementation>();

            containerRegistry.RegisterForNavigation<NavigationPage>();
            containerRegistry.RegisterForNavigation<MainPage, MainPageViewModel>();
            containerRegistry.RegisterForNavigation<LicensePage, LicensePageViewModel>();
            containerRegistry.RegisterForNavigation<WifiPage, WifiPageViewModel>();
            containerRegistry.RegisterForNavigation<HomePage, HomePageViewModel>();
            containerRegistry.RegisterForNavigation<SelectVasePage, SelectVasePageViewModel>();
            containerRegistry.RegisterForNavigation<MasterPage, MasterPageViewModel>();
            containerRegistry.RegisterForNavigation<AppNavigationPage, AppNavigationPageViewModel>();
            containerRegistry.RegisterForNavigation<HistoricPage, HistoricPageViewModel>();
            containerRegistry.RegisterForNavigation<ImagesPage, ImagesPageViewModel>();
            containerRegistry.RegisterForNavigation<ConfigPage, ConfigPageViewModel>();
        }
    }
}
