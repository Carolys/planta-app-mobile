﻿using EC0.Mocks;
using EC0.Models;
using EC0.Services;
using Prism.Commands;
using Prism.Mvvm;
using Prism.Navigation;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace EC0.ViewModels
{
    public class HistoricPageViewModel : ViewModelBase, INavigationAware
    {
        public ClientApi Api { get; set; }

        public Command NavigateSelectVase { get; set; }

        private ObservableCollection<Indicator> _indicatorsHumidity;
        public ObservableCollection<Indicator> IndicatorsHumidity
        {
            get { return _indicatorsHumidity; }
            set { SetProperty(ref _indicatorsHumidity, value); }
        }

        private ObservableCollection<Indicator> _indicatorsTemperature;
        public ObservableCollection<Indicator> IndicatorsTemperature
        {
            get { return _indicatorsTemperature; }
            set { SetProperty(ref _indicatorsTemperature, value); }
        }

        private int _vaseId;
        public int VaseId
        {
            get
            {
                return _vaseId;
            }
            set
            {
                SetProperty(ref _vaseId, value);
                RaisePropertyChanged("VaseId");
            }
        }

        private int _vase;
        public int Vase
        {
            get
            {
                return _vase;
            }
            set
            {
                SetProperty(ref _vase, value);
                RaisePropertyChanged("Vase");
            }
        }

        private bool _isLoading;
        public bool IsLoading
        {
            get { return _isLoading; }
            set
            {
                { SetProperty(ref _isLoading, value); }
            }
        }
        public MockIndicators mockIndicators { get; set; }
        public HistoricPageViewModel(INavigationService navigationService) : base(navigationService)
        {
            IndicatorsHumidity = new ObservableCollection<Indicator>();
            IndicatorsTemperature = new ObservableCollection<Indicator>();

            NavigateSelectVase = new Command(async (obj) => await ExecuteNavigateSelectVase(obj));
            Api = ClientApi;
            VaseId = IdVase;
            mockIndicators = new MockIndicators();
            Plant = new Models.Plant();
            Plant.Indicators = mockIndicators.getData();

            //MessagingCenter.Subscribe<string>(this, "id",
            //    (id) =>
            //    {
            //        VaseId = Convert.ToInt32(id);
            //    });
            //MessagingCenter.Subscribe<ClientApi>(this, "api",
            //    (api) =>
            //    {
            //        Api = api;                    
            //    });
            GetIndicators();
        }

        private async Task GetIndicators()
        {
            IsLoading = true;
            try
            {
                //var result = await Api.PlantService.GetPlantAsync(false, VaseId);
                var result = Plant.Indicators;
                if (result != null)
                {
                    //Api.Cache.Plants[VaseId - 1] = result.Body;
                    if(result.Count > 0)
                    {
                        foreach (var item in result)
                        {
                            item.DateTime_Created_at = DateTime.ParseExact(item.Created_at, "yyyy-MM-dd'T'HH:mm:ss'.000000Z'", System.Globalization.CultureInfo.InvariantCulture);
                            if (item.Type == Models.Type.Temperature)
                                IndicatorsTemperature.Add(item);
                            else
                                IndicatorsHumidity.Add(item);

                        }
                    }
                }
                else
                {
                    await Application.Current.MainPage.DisplayAlert("MENSAGEM", "Não foi possível obter os dados da sua planta. Por favor tente novamente mais tarde.", "Ok");
                }
            }
            catch (Exception e)
            {
                await Application.Current.MainPage.DisplayAlert("ERRO", "Ocorreu um erro ao obter os dados da planta. Por favor tente novamente mais tarde.", "Ok");
            }
            ClientApi = Api;
            //MessagingCenter.Send<ClientApi>(this.Api, "api");
            IsLoading = false;
        }

        private async Task ExecuteNavigateSelectVase(object obj)
        {
            var parameter = new NavigationParameters();
            parameter.Add("Api", Api);
            await NavigationService.NavigateAsync("SelectVasePage", parameter);
        }
    }
}
