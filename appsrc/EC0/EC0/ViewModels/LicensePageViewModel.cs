﻿using EC0.Services;
using Prism.Commands;
using Prism.Mvvm;
using Prism.Navigation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace EC0.ViewModels
{
    public class LicensePageViewModel : ViewModelBase
    {
        public ClientApi Api { get; set; }
        public Command NavigateWifi { get; set; }

        private string _code;
        public string Code
        {
            get
            {
                return _code;
            }
            set
            {
                SetProperty(ref _code, value);
                RaisePropertyChanged("Code");
            }
        }
        public LicensePageViewModel(INavigationService navigationService) : base(navigationService)
        {
            ClientApi api = new ClientApi();
            Api = api;

            NavigateWifi = new Command(async (obj) => await ExecuteNavigateWifi(obj));
        }

        private async Task ExecuteNavigateWifi(object obj)
        {
            if (Code == "f8sIafIXqS")
            {
                var parameter = new NavigationParameters();
                parameter.Add("Api", Api);
                await NavigationService.NavigateAsync("WifiPage", parameter);
            }
            else
            {
                await Application.Current.MainPage.DisplayAlert("MENSAGEM", "Licença incorreta! Por favor, insira novamente ou tente mais tarde.", "Ok");
            }
        }
    }
}
