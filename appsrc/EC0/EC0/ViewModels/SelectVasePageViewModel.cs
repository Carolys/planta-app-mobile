﻿using EC0.Services;
using Prism.Commands;
using Prism.Mvvm;
using Prism.Navigation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace EC0.ViewModels
{
    public class SelectVasePageViewModel : ViewModelBase, INavigationAware
    {
        public ClientApi Api { get; set; }
        public Command NavigatePlant1 { get; set; }
        public Command NavigatePlant2 { get; set; }
        public void OnNavigatedTo(INavigationParameters parameters)
        {
            Api = parameters.GetValue<ClientApi>("Api");
        }

        public SelectVasePageViewModel(INavigationService navigationService)
            : base(navigationService)
        {
            Title = "Select Vase Page";
            NavigatePlant1 = new Command(async (obj) => await ExecuteNavigatePlant(1));
            NavigatePlant2 = new Command(async (obj) => await ExecuteNavigatePlant(2));
        }

        private async Task ExecuteNavigatePlant(int plant)
        {
            var parameter = new NavigationParameters();
            parameter.Add("Planta", plant);
            parameter.Add("Api", Api);
            await NavigationService.NavigateAsync("/MasterPage/AppNavigationPage/HomePage", parameter);
        }
    }
}

