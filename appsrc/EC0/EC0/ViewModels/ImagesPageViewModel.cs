﻿using EC0.Mocks;
using EC0.Models;
using Prism.Commands;
using Prism.Mvvm;
using Prism.Navigation;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

namespace EC0.ViewModels
{
    public class ImagesPageViewModel : ViewModelBase
    {
        private ObservableCollection<Image> _images;
        public ObservableCollection<Image> Images
        {
            get { return _images; }
            set { SetProperty(ref _images, value); }
        }
        public MockImages mockImages { get; set; }
        public ImagesPageViewModel(INavigationService navigationService) : base(navigationService)
        {
            mockImages = new MockImages();
            Images = mockImages.getData();
        }
    }
}
