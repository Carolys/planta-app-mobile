﻿using Prism.Commands;
using Prism.Mvvm;
using Prism.Navigation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace EC0.ViewModels
{
    public class ConfigPageViewModel : ViewModelBase
    {
        public Command Configure { get; set; }

        private string _quant;
        public string Quant
        {
            get
            {
                return _quant;
            }
            set
            {
                SetProperty(ref _quant, value);
                RaisePropertyChanged("Quant");
            }
        }
        public ConfigPageViewModel(INavigationService navigationService) : base(navigationService)
        {
            Configure = new Command(async (obj) => await ExecuteConfigure(obj));
        }

        private async Task ExecuteConfigure(object obj)
        {
            if(string.IsNullOrWhiteSpace(Quant))
                await Application.Current.MainPage.DisplayAlert("MENSAGEM", "Verifique os campos e tente novamente!", "Ok");
            else
                await Application.Current.MainPage.DisplayAlert("MENSAGEM", "Quantidade de regas configuradas com sucesso!", "Ok");
        }
    }
}
