﻿using Prism.Commands;
using Prism.Mvvm;
using Prism.Navigation;
using Prism.Services;
using System;
using System.Collections.Generic;
using System.Linq;

namespace EC0.ViewModels
{
    public class MasterPageViewModel : ViewModelBase
    {
        private string _photo;
        public string Photo
        {
            get
            {
                return _photo;
            }
            set
            {
                SetProperty(ref _photo, value);
                RaisePropertyChanged("Photo");
            }
        }

        private string _name;
        public string Name
        {
            get
            {
                return _name;
            }
            set
            {
                SetProperty(ref _name, value);
                RaisePropertyChanged("Name");
            }
        }

        private string _email;
        public string Email
        {
            get
            {
                return _email;
            }
            set
            {
                SetProperty(ref _email, value);
                RaisePropertyChanged("Email");
            }
        }
        public IPageDialogService _pageDialogService { get; }

        public MasterPageViewModel(IPageDialogService pageDialogService, INavigationService navigationService) : base(navigationService)
        {
            _pageDialogService = pageDialogService;

            getProfileData();
        }

        private void getProfileData()
        {
            Photo = "planticon.png";
            Name = "usuário";
        }
    }
}
