﻿using EC0.Services;
using Prism.Commands;
using Prism.Mvvm;
using Prism.Navigation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace EC0.ViewModels
{
    public class WifiPageViewModel : ViewModelBase, INavigationAware
    {
        public ClientApi Api { get; set; }
        public Command NavigateHome { get; set; }
        public void OnNavigatedTo(INavigationParameters parameters)
        {
            Api = parameters.GetValue<ClientApi>("Api");
        }
        public WifiPageViewModel(INavigationService navigationService) : base(navigationService)
        {
            NavigateHome = new Command(async (obj) => await ExecuteNavigateHome(obj));
        }

        private async Task ExecuteNavigateHome(object obj)
        {
            //await NavigationService.NavigateAsync("/MasterPage/AppNavigationPage/HomePage");
            var parameter = new NavigationParameters();
            parameter.Add("Api", Api);
            await NavigationService.NavigateAsync("SelectVasePage", parameter);
        }
    }
}
