﻿using EC0.Mocks;
using EC0.Services;
using Prism.Commands;
using Prism.Mvvm;
using Prism.Navigation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace EC0.ViewModels
{
    public class HomePageViewModel : ViewModelBase, INavigationAware
    {
        public ClientApi Api { get; set; }

        public Command NavigateSelectVase { get; set; }

        private int _vaseId;
        public int VaseId
        {
            get
            {
                return _vaseId;
            }
            set
            {
                SetProperty(ref _vaseId, value);
                RaisePropertyChanged("VaseId");
            }
        }

        private int _vase;
        public int Vase
        {
            get
            {
                return _vase;
            }
            set
            {
                SetProperty(ref _vase, value);
                RaisePropertyChanged("Vase");
            }
        }

        private string _temperature;
        public string Temperature
        {
            get
            {
                return _temperature;
            }
            set
            {
                SetProperty(ref _temperature, value);
                RaisePropertyChanged("Temperature");
            }
        }

        private string _humidity;
        public string Humidity
        {
            get
            {
                return _humidity;
            }
            set
            {
                SetProperty(ref _humidity, value);
                RaisePropertyChanged("Humidity");
            }
        }

        private string _wateringQuantity;
        public string WateringQuantity
        {
            get
            {
                return _wateringQuantity;
            }
            set
            {
                SetProperty(ref _wateringQuantity, value);
                RaisePropertyChanged("WateringQuantity");
            }
        }

        private bool _isLoading;
        public bool IsLoading
        {
            get { return _isLoading; }
            set
            {
                { SetProperty(ref _isLoading, value); }
            }
        }
        public MockIndicators mockIndicators { get; set; }
        public void OnNavigatedTo(INavigationParameters parameters)
        {
            int vaso = parameters.GetValue<int>("Planta");
            Api = parameters.GetValue<ClientApi>("Api");
            //MessagingCenter.Send<ClientApi>(this.Api, "api");
            //MessagingCenter.Send<string>(vaso.ToString(), "id");
            ClientApi = Api;
            if (vaso != 0)
            {
                VaseId = vaso;
                IdVase = vaso;
                //GetIndicators();
                mockIndicators = new MockIndicators();
                Plant = new Models.Plant();
                Plant.Indicators = mockIndicators.getData();
                Temperature = Plant.Indicators.LastOrDefault(x => x.Type.ToString() == Models.Type.Temperature.ToString()).Value.ToString();
                Humidity = Plant.Indicators.LastOrDefault(x => x.Type.ToString() == Models.Type.Humidity.ToString()).Value.ToString();
                WateringQuantity = "2";
            }
        }

        public HomePageViewModel(INavigationService navigationService) : base(navigationService)
        {
            NavigateSelectVase = new Command(async (obj) => await ExecuteNavigateSelectVase(obj));
            if(VaseId == 0)
            {
                Api = ClientApi;
                VaseId = IdVase;
            }
        }

        private async Task GetIndicators()
        {
            IsLoading = true;
            try
            {
                var result = await Api.PlantService.GetPlantAsync(false, VaseId);

                if (result != null)
                {
                    Api.Cache.Plants[VaseId - 1] = result.Body;
                    Plant = result.Body;
                    Temperature = result.Body.Indicators.LastOrDefault(x => x.Type.ToString() == Models.Type.Temperature.ToString()).Value.ToString();
                    Humidity = result.Body.Indicators.LastOrDefault(x => x.Type.ToString() == Models.Type.Humidity.ToString()).Value.ToString();
                    WateringQuantity = result.Body.Next_run_in_minutes.ToString();
                }
                else
                {
                    Temperature = "?";
                    Humidity = "?";
                    WateringQuantity = "?";
                    await Application.Current.MainPage.DisplayAlert("MENSAGEM", "Não foi possível obter os dados da sua planta. Por favor tente novamente mais tarde.", "Ok");
                }
            }
            catch (Exception e)
            {
                await Application.Current.MainPage.DisplayAlert("ERRO", "Ocorreu um erro ao obter os dados da planta. Por favor tente novamente mais tarde.", "Ok");
            }
            //MessagingCenter.Send<ClientApi>(this.Api, "api");
            ClientApi = Api;
            IsLoading = false;
        }

        private async Task ExecuteNavigateSelectVase(object obj)
        {
            var parameter = new NavigationParameters();
            parameter.Add("Api", Api);
            await NavigationService.NavigateAsync("SelectVasePage", parameter);
        }
    }
}
