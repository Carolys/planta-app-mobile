﻿using Xamarin.Forms;

namespace EC0.Views
{
    public partial class LicensePage : ContentPage
    {
        public LicensePage()
        {
            InitializeComponent();
            if ((Application.Current.MainPage as MasterDetailPage) != null)
            {
                (Application.Current.MainPage as MasterDetailPage).IsGestureEnabled = false;
            }
        }
    }
}
