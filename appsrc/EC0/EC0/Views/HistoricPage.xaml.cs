﻿using System;
using Xamarin.Forms;

namespace EC0.Views
{
    public partial class HistoricPage : ContentPage
    {
        public HistoricPage()
        {
            InitializeComponent();

            listTemperature.IsVisible = false;
            listHumidity.IsVisible = true;
            boxViewColor.BackgroundColor = Color.FromHex("#8DF1FF");
            backgroundImg.Source = "background_blue.png";
        }

        public async void OnHumidityTapped(object sender, EventArgs args)
        {
            listTemperature.IsVisible = false;
            listHumidity.IsVisible = true;
            boxViewColor.BackgroundColor = Color.FromHex("#8DF1FF");
            backgroundImg.Source = "background_blue.png";
        }

        public async void OnTemperatureTapped(object sender, EventArgs args)
        {
            listTemperature.IsVisible = true;
            listHumidity.IsVisible = false;
            boxViewColor.BackgroundColor = Color.FromHex("#FFDC94");
            backgroundImg.Source = "background_yellow.png";
        }
    }
}
