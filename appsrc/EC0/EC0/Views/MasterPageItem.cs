﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EC0.Views
{
    public class MasterPageItem
    {
        public string Titulo { get; set; }
        public string Item { get; set; }
        public Type TargetType { get; set; }
        public string Image { get; set; }
    }
}
