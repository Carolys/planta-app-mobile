﻿using EC0.Models;
using System;
using System.Collections.Generic;
using Xamarin.Forms;

namespace EC0.Views
{
    public partial class MasterPage : MasterDetailPage
    {
        Dictionary<int, NavigationPage> MenuPages = new Dictionary<int, NavigationPage>();
        public MasterPage()
        {
            InitializeComponent();
            MenuPages.Add((int)MenuItemType.Browse, (NavigationPage)Detail);
            listaperfil.ItemSelected += OnItemSelected;
        }

        void OnItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            var item = e.SelectedItem as MasterPageItem;
            if (item != null)
            {
                Detail = new NavigationPage((Page)Activator.CreateInstance(item.TargetType));
                listaperfil.SelectedItem = null;
                IsPresented = false;
            }
        }
    }
}
