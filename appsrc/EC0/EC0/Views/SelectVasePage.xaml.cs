﻿using Xamarin.Forms;

namespace EC0.Views
{
    public partial class SelectVasePage : ContentPage
    {
        public SelectVasePage()
        {
            InitializeComponent();
            if ((Application.Current.MainPage as MasterDetailPage) != null)
            {
                (Application.Current.MainPage as MasterDetailPage).IsGestureEnabled = false;
            }
        }
    }
}
