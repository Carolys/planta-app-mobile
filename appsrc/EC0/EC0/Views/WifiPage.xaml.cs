﻿using Xamarin.Forms;

namespace EC0.Views
{
    public partial class WifiPage : ContentPage
    {
        public WifiPage()
        {
            InitializeComponent();
            if ((Application.Current.MainPage as MasterDetailPage) != null)
            {
                (Application.Current.MainPage as MasterDetailPage).IsGestureEnabled = false;
            }
        }
    }
}
