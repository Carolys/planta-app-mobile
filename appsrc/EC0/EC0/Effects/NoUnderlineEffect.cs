﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace EC0.Effects
{
    public class NoUnderlineEffect : RoutingEffect
    {
        public NoUnderlineEffect() : base("EC0.NoUnderlineEffect")
        {
        }
    }
}
