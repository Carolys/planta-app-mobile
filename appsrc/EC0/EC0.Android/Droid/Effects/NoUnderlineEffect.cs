﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Graphics.Drawables;
using Android.Widget;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ResolutionGroupName("EC0")]
[assembly: ExportEffect(typeof(EC0.Droid.Droid.Effects.NoUnderlineEffect), nameof(EC0.Effects.NoUnderlineEffect))]
namespace EC0.Droid.Droid.Effects
{
    public class NoUnderlineEffect : PlatformEffect
    {
        protected override void OnAttached()
        {
            if (this.Control != null)
            {
                GradientDrawable gd = new GradientDrawable();
                gd.SetColor(global::Android.Graphics.Color.Transparent);
                this.Control.SetBackgroundDrawable(gd);
                if (this.Control is EditText editText)
                {
                    editText.SetRawInputType(Android.Text.InputTypes.TextFlagNoSuggestions);
                }

            }
        }
        protected override void OnDetached()
        {
        }
    }
}